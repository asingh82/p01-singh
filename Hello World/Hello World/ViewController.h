//
//  ViewController.h
//  Hello World
//
//  Created by Anshima on 23/01/17.
//  Copyright © 2017 Anshima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel *helloWorldLabel;

-(IBAction)clicked:(id)hello1;

@end


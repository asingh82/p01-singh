//
//  ViewController.m
//  Hello World
//
//  Created by Anshima on 23/01/17.
//  Copyright © 2017 Anshima. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize helloWorldLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)clicked:(id)hello1{
    [helloWorldLabel setText:@"Anshima Singh"];
    [helloWorldLabel setTextColor:[UIColor cyanColor]];
    [helloWorldLabel setBackgroundColor:[UIColor blackColor]];
    [helloWorldLabel setFont:[UIFont fontWithName:@"Apple Garamond" size:18]];
}

@end
